function criar_Div(onde, id, classe) {
    const div = document.createElement('div')
    div.classList.add(classe)
    div.id = id
    document.getElementById(onde).appendChild(div)
}

function criar_Button(onde, text, id, classe) {
    const button = document.createElement('button')
    button.id = id
    button.classList.add(classe)
    button.innerHTML = text
    document.getElementById(onde).appendChild(button)
}

function button_Reset(player) {
    document.getElementById('jogo').innerHTML = ''
    criar_Div('jogo', 'coluna', 'coluna')
    criar_Div('coluna', 'venceu', 'venceu')
    const p = document.createElement('p')
    const p_in = document.createTextNode(`${player} venceram !!!`)
    p.appendChild(p_in)
    document.getElementById('venceu').appendChild(p)

    const win = document.getElementById('venceu')

    criar_Button('venceu', 'Começar de novo', 'reset', 'reset')
    const button = document.getElementById('reset')
    button.addEventListener('click', function() {
        win.classList.toggle('fade')
        criar_Div('body', 'inv', 'inv')
        setTimeout(function() {
            StartGame()
            del_inv()
        }, 1500)
    })
    const msg = document.getElementById('player')
    if (msg === null) {
        return
    }
    msg.parentNode.removeChild(msg)
}

function delect() {
    const div = document.getElementById('venceu')
    if (div === null) {
        return
    }
    div.parentNode.removeChild(div)
}


criar_Div('body', 'intro', 'intro')

const h1 = document.createElement('h1')
const h1_in = document.createTextNode('Lig-4')
h1.appendChild(h1_in)
document.getElementById('intro').appendChild(h1)

criar_Div('intro', 'tutorial', 'tutorial')
criar_Button('intro', 'Sobre o jogo', 'myBtn', 'myBtn')
criar_Div('tutorial', 'myModal', 'modal')
criar_Div('myModal', 'modal-content', 'modal-content')

const span1 = document.createElement('span')
span1.classList.add('close')
span1.innerHTML = '&times;'
document.getElementById('modal-content').appendChild(span1)

const par = document.createElement('p')
const par_in = document.createTextNode('No Lig-4, um jogador assume a cor Vermelha e o outro assume a cor Preta. Os jogadores se alternam inserindo suas peças em uma das 7 colunas de uma tabela 7x6. O primeiro jogador que conseguir quatro de suas peças em uma linha (seja horizontal, vertical ou diagonal) vence.')
par.appendChild(par_in)
document.getElementById('modal-content').appendChild(par)

criar_Button('intro', 'Play', 'button', 'button')

criar_Div('body', 'jogo', 'jogo')

// Get the modal
var modal = document.getElementById("myModal");

// Get the button that opens the modal
var btn = document.getElementById("myBtn");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks on the button, open the modal
btn.onclick = function() {
    modal.style.display = "block";
}

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
    modal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}


const button = document.getElementById("button");
button.addEventListener("click", () => {
    const intro = document.getElementById('intro')
    intro.parentNode.removeChild(intro)
    StartGame();
});