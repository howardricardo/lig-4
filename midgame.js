//------------------------Função usada no start.js---------------------------------------------
function StartGame() {
    //---------------------
    delect()
    contador = 0
    let linha = document.createElement('div')
    linha.classList.add('coluna')
    linha.id = 'coluna'
    document.getElementById('jogo').appendChild(linha)
    //---------------------

    let player = document.createElement('div') // Aqui é onde vai a mensagem "É a vez do jogador: 'x'"
    player.id = 'player'
    player.classList.add('player')
    let p = document.createElement('p')
    p.id = 'vez'
    let player_text = document.createTextNode('É a vez das pretas')
    let bolinha = document.createElement('div')
    bolinha.id = 'bolinha'
    bolinha.classList.add('bolinha_preta')

    player.appendChild(bolinha)
    p.appendChild(player_text)
    player.appendChild(p)
    document.body.appendChild(player)


    for (let i = 0; i < 7; i++) { // Cria uma div para cada coluna.
        let div = document.createElement('div')
        div.id = i
        div.classList.add('block')
        document.getElementById('coluna').appendChild(div)
    }

    StartClick()
}
//----------------Função que dá interatividade ao jogo-----------------------------------------
function StartClick() {
    const Peças = document.querySelectorAll('#coluna > div')
    let ClickDiv
    let valid = true // Quando true, a peça é preta e o jogador 1 que joga. Quando false, a peça é vermelha e o jogador 2 que joga.

    Peças.forEach(element => {
        element.addEventListener('click', handlerClick)
    })

    function handlerClick(event) {

        ClickDiv = event.currentTarget // Armazena a div clicada
        Div = document.getElementById(ClickDiv.id)
        let piece_qntd = ClickDiv.childNodes.length // Aqui é a quantidade de peças que tem em cada div clicada

        if (piece_qntd < 6) { // Isso limita a quantidade de peças por coluna, que deve ser 6.

            const Div = document.createElement('div')
            document.getElementById(ClickDiv.id).appendChild(Div)

            if (valid) {

                const jogador = document.getElementById('player')
                const vez = document.getElementById('vez')
                vez.classList.remove('player')
                vez.classList.add('player_red')
                valid = false
                Div.classList.add('peça')
                vez.innerHTML = 'É a vez das vermelhas'

                const bola = document.getElementById('bolinha')
                bola.classList.remove('bolinha_preta')
                bola.classList.add('bolinha_vermelha')

            } else {

                const jogador = document.getElementById('player')

                const vez = document.getElementById('vez')
                vez.classList.remove('player_red')
                vez.classList.add('player')

                valid = true
                Div.classList.add('peça2')
                vez.innerHTML = 'É a vez das pretas'

                const bola = document.getElementById('bolinha')
                bola.classList.remove('bolinha_vermelha')
                bola.classList.add('bolinha_preta')

            }
            validation_column(ClickDiv)
            validation_row()
            validation_diag()
            validation_diag_dec()
            is_empate()
        }
    }
}
//----------------------------------------------------------------------------------