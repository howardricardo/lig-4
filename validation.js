function black_or_red (array, linha) {
    if (array[0][linha].className === 'peça') {
        criar_Div('body', 'inv', 'inv')
        setTimeout(function () {
            button_Reset('Pretas')
            del_inv()
        }, 3000)
    }
    if (array[0][linha].className === 'peça2') {
        criar_Div('body', 'inv', 'inv')
        setTimeout(function () {
            button_Reset('Vermelhas')
            del_inv()
        }, 3000)
    }
}
/**************************************************************************************************************************************************/

/*As funções são chamadas na função handlerClick;
 
A cada clique é feito a validação de vitória;
 
Quando as funções são chamadas, recebem como parâmetro a variável ClickDiv,
essa variável retorna a coluna que foi clicada, a partir disso, a função de validação usa esse parâmetro para
retornar os filhos em um array, usando o childNodes.
*/

function del_inv() {
    const inv = document.getElementById('inv')
    inv.parentNode.removeChild(inv)
}


function validation_column(ClickDiv) {
    let column_children = ClickDiv.childNodes

    let counter_black = []
    let counter_red = []

    for (let i = 0; i < column_children.length; i++) { //Percorre todo o array com os filhos da coluna, e incrementa o contador.
        if (column_children[i].className === 'peça2') { //Caso a primeira bola seja vermelha, o contador não fica negativo, para não bugar.
            counter_black = []
            counter_red.push(column_children[i])
        } else {
            counter_red = []
            counter_black.push(column_children[i])
        }
    }

    if (counter_black.length === 4) { 
        function win_black(i) {
            counter_black[i].classList.remove('peça')
            counter_black[i].classList.add('peçaWin_black')
        }
        for (let i = 0; i < counter_black.length; i++) {
            setTimeout(function() {win_black(i)}, 1500)
        }
        criar_Div('body', 'inv', 'inv')
        setTimeout(function () {
            button_Reset('Pretas')
            del_inv()
        }, 3000) 
    }
    if (counter_red.length === 4)   {
        function win_red(i) {
            counter_red[i].classList.remove('peça2')
            counter_red[i].classList.add('peçaWin_red')
        }
        for (let i = 0; i < counter_red.length; i++) {
            setTimeout(function() {win_red(i)}, 1500)
        }
        criar_Div('body', 'inv', 'inv')
        setTimeout(function () {
            button_Reset('Vermelhas')
            del_inv()
        }, 3000)
    }
}


function validation_horizontal(linha, inicio, fim) {
    let pieces = [] // Tenho essa array vazia
    let win = []

    for (let i = inicio; i <= fim; i++) {  // pra cada coluna, coloca dentro da minha array, os filhos dela
        let peça = document.getElementById(i)
        if (peça === null) {
            return
        }
        pieces.push(peça.childNodes)
    }
    // a gente já tem uma array com os filhos 

    if (pieces[0][linha] === undefined || pieces[1][linha] === undefined || pieces[2][linha] === undefined || pieces[3][linha] === undefined) { // Aqui ele verifica se o espaço tá vazio. Se estiver vazio, a função acaba aqui.
        return true
    }
    if (pieces[0][linha].className === pieces[1][linha].className && pieces[0][linha].className === pieces[2][linha].className && pieces[0][linha].className === pieces[3][linha].className) {
        black_or_red(pieces, linha)
        win.push(pieces[0][linha], pieces[1][linha], pieces[2][linha], pieces[3][linha])
        function win_black(i) {
            win[i].classList.remove('peça')
            win[i].classList.add('peçaWin_black')
        }
        
        function win_red(i) {
            win[i].classList.remove('peça2')
            win[i].classList.add('peçaWin_red')
        }
        for (let i = 0; i < win.length; i++) {
            if (win[i].className === 'peça') {
                setTimeout(function() {win_black(i)}, 1500)
            }
            if (win[i].className === 'peça2') {
                setTimeout(function() {win_red(i)}, 1500)
            }
        }
    }
}

function validation_row() {
    for (let i = 0; i <= 6; i++) { // Aqui ele acessa todas as linhas por vez. Da coluna 0 até a 3, depois da coluna 1 até 4, depois da coluna 2 até 5, depois da coluna 3 até a 6.
        let inicio = 0
        let fim = 3
        while (inicio <= 3 && fim <= 6) {
            validation_horizontal(i, inicio, fim)
            inicio++
            fim++
        }
    }
}

function validation_diag() {
    let inicio = 0
    let fim = 3
    while (inicio <= 3 && fim <= 6) {
        diagonal(0, inicio, fim, 1, 2, 3)
        diagonal(1, inicio, fim, 2, 3, 4)
        diagonal(2, inicio, fim, 3, 4, 5)
        inicio++
        fim++
    }
}

function validation_diag_dec() {
    let inicio = 0
    let fim = 3
    while (inicio <= 3 && fim <= 6) {
        diagonal(5, inicio, fim, 4, 3, 2)
        diagonal(4, inicio, fim, 3, 2, 1)
        diagonal(3, inicio, fim, 2, 1, 0)
        inicio++
        fim++
    }
}

function diagonal(linha, inicio, fim, acima, acima2, acima3) {
    let pieces = []
    let win = []
    for (let i = inicio; i <= fim; i++) {
        let peça = document.getElementById(i)
        if (peça === null) {
            return
        }
        pieces.push(peça.childNodes)
    }
    if (pieces[0][linha] === undefined || pieces[1][acima] === undefined || pieces[2][acima2] === undefined || pieces[3][acima3] === undefined) { // Aqui ele verifica se o espaço tá vazio. Se estiver vazio, a função acaba aqui.
        return true
    }
    if (pieces[0][linha].className === pieces[1][acima].className && pieces[0][linha].className === pieces[2][acima2].className && pieces[0][linha].className === pieces[3][acima3].className) {
        black_or_red(pieces, linha)
        win.push(pieces[0][linha], pieces[1][acima], pieces[2][acima2], pieces[3][acima3])
        function win_black(i) {
            win[i].classList.remove('peça')
            win[i].classList.add('peçaWin_black')
        }
        
        function win_red(i) {
            win[i].classList.remove('peça2')
            win[i].classList.add('peçaWin_red')
        }
        for (let i = 0; i < win.length; i++) {
            if (win[i].className === 'peça') {
                setTimeout(function() {win_black(i)}, 1500)
            }
            if (win[i].className === 'peça2') {
                setTimeout(function() {win_red(i)}, 1500)
            }
        }
    }
}

let contador = 0

function is_empate() {
    contador++
    if (contador === 42) {
        setTimeout(function () {button_Reset('Empate! Nem as pretas nem as vermelhas')}, 1500)
    }
} 
